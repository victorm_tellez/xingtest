package com.me.xingtest;

import com.me.xingtest.api.RetrofitClient;
import com.me.xingtest.model.Repository;
import com.me.xingtest.ui.IMainView;
import com.me.xingtest.ui.MainPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Víctor Téllez on 05/12/2017.
 */
public class RepositoryShould {

    @Mock
    private IMainView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getData() {
        MainPresenter mainPresenter = new MainPresenter(view);
        mainPresenter.getRepositoriesRx(1);
    }

    @Test
    public void showData() {
        // Next text mock api
        RetrofitClient retrofitClient =  Mockito.mock(RetrofitClient.class);
        List<Repository> repositories = new ArrayList<>();
        Mockito.doReturn(repositories).when(retrofitClient).getClientRx("https://api.github.com/");
        // Get repo list and show in the view ...
    }


}
