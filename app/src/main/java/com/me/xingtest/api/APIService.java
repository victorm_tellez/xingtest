package com.me.xingtest.api;

import com.me.xingtest.model.Repository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Víctor Téllez on 01/12/2017.
 */

public interface APIService {

    @GET("users/xing/repos?per_page=10&access_token=eac2cbdff3302e893246b5779d09a350acd4c284")
    Call<List<Repository>> readRepositories(@Query("page") int page);


    @GET("users/xing/repos?per_page=10&access_token=eac2cbdff3302e893246b5779d09a350acd4c284")
    Observable<List<Repository>> readRepositoriesRx(@Query("page") int page);
}
