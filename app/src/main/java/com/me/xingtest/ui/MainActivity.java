package com.me.xingtest.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.me.xingtest.R;
import com.me.xingtest.model.Repository;

import java.util.List;

public class MainActivity extends AppCompatActivity implements IMainView {

    private final int PAGE_SIZE = 10;

    /**
     * Presenter
     */
    private IMainPresenter presenter;

    /**
     * Views
     */
    private RecyclerView recyclerview;
    private LinearLayoutManager layoutManager;

    private int page = 1;
    private boolean isLoading = true;
    private boolean isLastPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureViews();
        presenter = new MainPresenter(this);
        presenter.getRepositoriesRx(page);
    }

    private void configureViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerview = findViewById(R.id.recyclerview);
        layoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(layoutManager);
        // Pagination
        RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // TODO: This logic can be improve
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        page += 1;
                        presenter.getRepositoriesRx(page);
                    }
                }
            }
        };
        recyclerview.addOnScrollListener(recyclerViewOnScrollListener);
    }

    @Override
    public void showRepositories(List<Repository> repositories) {
        if (repositories.size() > 0){
            MyRecyclerAdapter adapter = new MyRecyclerAdapter(repositories, R.layout.recycler_item, this);
            recyclerview.setAdapter(adapter);
            isLoading = false;
        }
    }
}
