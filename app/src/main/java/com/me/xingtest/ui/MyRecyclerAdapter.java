package com.me.xingtest.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.me.xingtest.R;
import com.me.xingtest.model.Repository;

import java.util.List;

/**
 * Created by Víctor Téllez on 05/12/2017.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    private List<Repository> items;
    private int itemLayout;
    private Context context;

    public MyRecyclerAdapter(List<Repository> items, int itemLayout, Context context) {
        this.items = items;
        this.itemLayout = itemLayout;
        this.context = context;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        final Repository item = items.get(position);
        holder.name.setText(item.getName());
        holder.description.setText(item.getDescription());
        holder.ownerLogin.setText(item.getOwner().getLogin());

        if (item.getFork()) {
            holder.itemView.setBackgroundColor(Color.GREEN);
        }

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showAlerDialog(item);
                return false;
            }
        });
        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        private TextView description;
        private TextView ownerLogin;

        private ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            description = (TextView) itemView.findViewById(R.id.description);
            ownerLogin = (TextView) itemView.findViewById(R.id.ownerLogin);
        }
    }

    private void showAlerDialog(final Repository repo) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(context.getString(R.string.go_github))
                .setMessage(context.getString(R.string.choose_resource))
                .setPositiveButton(context.getString(R.string.repo_url), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        goToBrowser(repo.getHtmlUrl());
                    }
                })
                .setNegativeButton(context.getString(R.string.user_url), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        goToBrowser(repo.getOwner().getHtmlUrl());
                    }
                })
                .show();
    }

    private void goToBrowser(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }
}