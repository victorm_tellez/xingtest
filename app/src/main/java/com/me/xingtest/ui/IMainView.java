package com.me.xingtest.ui;

import com.me.xingtest.model.Repository;

import java.util.List;

/**
 * Created by Víctor Téllez on 01/12/2017.
 */

public interface IMainView {
    void showRepositories(List<Repository> repositories);
}
