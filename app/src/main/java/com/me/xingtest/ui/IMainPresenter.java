package com.me.xingtest.ui;

/**
 * Created by Víctor Téllez on 01/12/2017.
 */

public interface IMainPresenter {
    void getRepositories(int page);
    void getRepositoriesRx(int page);
}
