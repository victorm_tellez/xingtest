package com.me.xingtest.ui;

import android.util.Log;

import com.me.xingtest.api.APIService;
import com.me.xingtest.model.Repository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.me.xingtest.api.RetrofitClient.getAPIServiceRx;

/**
 * Created by Víctor Téllez on 01/12/2017.
 */

public class MainPresenter implements IMainPresenter {

    private IMainView view;

    public MainPresenter(IMainView view) {
        this.view = view;
        mAPIService = getAPIServiceRx();
    }

    private APIService mAPIService;

    @Override
    public void getRepositories(int page) {
        mAPIService.readRepositories(page).enqueue(new Callback<List<Repository>>() {
            @Override
            public void onResponse(Call<List<Repository>> call, Response<List<Repository>> response) {

                if(response.isSuccessful()) {
                    showRepositories(response.body());
                    Log.d(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<List<Repository>> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    @Override
    public void getRepositoriesRx(int page) {
        // RxJava
        mAPIService.readRepositoriesRx(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<List<Repository>>() {

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Rx error: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "Rx onCompleted");
                    }

                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "Rx onSubscribe");
                    }

                    @Override
                    public void onNext(List<Repository> repositories) {
                        Log.d(TAG, "Rx onNext");
                        showRepositories(repositories);
                    }
                });
    }

    public void showRepositories(List<Repository> repositories) {
        view.showRepositories(repositories);
    }
}
